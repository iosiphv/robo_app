package com.view.helper;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.view.R;

public class RCIncomeDocHeadItemSwipe extends ItemTouchHelper.SimpleCallback {
	private final Paint p;
	private final Bitmap iconEdit;
	private final Bitmap iconDel;
	private OnSwipedListener onSwipedListener;
	private String TAG = "RCIncomeDocHeadItemSwipe";

	public RCIncomeDocHeadItemSwipe(OnSwipedListener onSwipedListener) {
		super(0, ItemTouchHelper.START | ItemTouchHelper.END);
		p = new Paint();
		iconEdit = getBitmapFromRes(android.R.drawable.ic_menu_edit);
		iconDel = getBitmapFromRes(android.R.drawable.ic_menu_delete);

		this.onSwipedListener = onSwipedListener;
	}


	public interface OnSwipedListener {
		void onSwiped(int direction, int pos);
	}

	private Bitmap getBitmapFromRes(int idRes) {
		return BitmapFactory.decodeResource(Resources.getSystem(), idRes);
	}

	@Override
	public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
		return makeMovementFlags(0, ItemTouchHelper.START | ItemTouchHelper.END);
	}

	@Override
	public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
		return makeMovementFlags(0, ItemTouchHelper.START | ItemTouchHelper.END);
	}

	@Override
	public boolean onMove(RecyclerView recyclerView,
	                      RecyclerView.ViewHolder viewHolder,
	                      RecyclerView.ViewHolder target) {
		return false;
	}

	@Override
	public boolean isLongPressDragEnabled() {
		return false;
	}

	@Override
	public boolean isItemViewSwipeEnabled() {
		return true;
	}

	@Override
	public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
		int position = viewHolder.getAdapterPosition();
		onSwipedListener.onSwiped(direction, position);
	}

	@Override
	public void onChildDraw(
			Canvas c,
			RecyclerView recyclerView,
			RecyclerView.ViewHolder viewHolder,
			float dX, float dY, int actionState, boolean isCurrentlyActive) {

		if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
			View view = viewHolder.itemView;
			int offset = view.getWidth() >> 2;

			if (Math.abs(dX) <= offset - 2)
				super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

			if (dX > 0 && dX <= offset) {
				int colorconfirm = ContextCompat.getColor(view.getContext(), R.color.colorConfirm);
				int rightOffset = (int) (view.getLeft() + dX);
				Rect rectE = new Rect(view.getLeft(), view.getTop(), rightOffset, view.getBottom());

				p.setColor(colorconfirm);
				c.drawRect(rectE, p);

				if (dX >= iconEdit.getHeight()) c
						.drawBitmap(iconEdit, null,
						            new Rect(
								            view.getLeft() + ((int) dX / 2) - iconEdit.getWidth() / 2,
								            view.getBottom() - rectE.height() / 2 - iconEdit.getHeight() / 2,
								            view.getLeft() + ((int) dX / 2) + iconEdit.getWidth() / 2,
								            view.getBottom() - rectE.height() / 2 + iconEdit.getHeight() / 2
						            ),
						            p);
			}

			if (dX < 0 && -dX <= offset) {
				int leftOffset = (int) (view.getRight() + dX);
				Rect rectToDel = new Rect(leftOffset, view.getTop(), view.getRight(), view.getBottom());
				int colornegative = ContextCompat.getColor(view.getContext(), R.color.colorNegative);
				p.setColor(colornegative);

				c.drawRect(rectToDel, p);

				if (-dX >= iconDel.getHeight()) c
						.drawBitmap(iconDel, null,
						            new Rect(
								            view.getRight() + ((int) dX / 2) - this.iconDel.getWidth() / 2,
								            view.getBottom() - rectToDel.height() / 2 - iconDel.getHeight() / 2,
								            view.getRight() + ((int) dX / 2) + iconDel.getWidth() / 2,
								            view.getBottom() - rectToDel.height() / 2 + iconDel.getHeight() / 2),
						            p);
			}
		} else clearView(recyclerView, viewHolder);
		Log.d(TAG, "onChildDraw: ");
	}

	@Override
	public void onChildDrawOver(Canvas c,
	                            RecyclerView recyclerView,
	                            RecyclerView.ViewHolder viewHolder,
	                            float dX,
	                            float dY,
	                            int actionState,
	                            boolean isCurrentlyActive) {
		super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
		Log.d(TAG, "onChildDrawOver: ");
	}

	@Override
	public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
		return 0.4f;
	}
}
