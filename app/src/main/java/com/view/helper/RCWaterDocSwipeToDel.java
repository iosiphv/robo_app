package com.view.helper;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.view.R;
import com.vm.docs.water.WaterDocsVM;

public class RCWaterDocSwipeToDel extends ItemTouchHelper.SimpleCallback {

	private final WaterDocsVM viewModel;
	private final Bitmap icon;
	private final Paint paint;

	public RCWaterDocSwipeToDel(WaterDocsVM viewModel) {
		super(0, ItemTouchHelper.START);
		icon = BitmapFactory
				.decodeResource(Resources.getSystem(), android.R.drawable.ic_menu_delete);
		paint = new Paint();
		this.viewModel = viewModel;
	}

	@Override
	public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
		return makeMovementFlags(0, ItemTouchHelper.START);
	}

	@Override
	public boolean onMove(RecyclerView recyclerView,
	                      RecyclerView.ViewHolder viewHolder,
	                      RecyclerView.ViewHolder target) {
		return false;
	}

	@Override
	public boolean isLongPressDragEnabled() {
		return false;
	}

	@Override
	public boolean isItemViewSwipeEnabled() {
		return true;
	}

	@Override
	public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
		if (direction == ItemTouchHelper.START) {
			Snackbar
					.make(viewHolder.itemView, R.string.process_delete_doc, Snackbar.LENGTH_SHORT)
					.show();
			if (viewModel.deleteDoc(viewHolder.getAdapterPosition()))
				Snackbar
						.make(viewHolder.itemView, R.string.process_delete_doc_end, Snackbar.LENGTH_SHORT)
						.show();
		}
	}

	@Override
	public void onChildDraw(
			Canvas c,
			RecyclerView recyclerView,
			RecyclerView.ViewHolder viewHolder,
			float dX, float dY,
			int actionState,
			boolean isCurrentlyActive) {
		View view = viewHolder.itemView;
		int leftOffset = view.getRight() >> 1;

		if (dX <= 0 & -dX <= leftOffset) {
			super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
			leftOffset = (int) (view.getRight() + dX);
		}

		paint.setColor(ContextCompat.getColor(view.getContext(), R.color.colorNegative));
		Rect rectToDel = new Rect(leftOffset, view.getTop(), view.getRight(), view.getBottom());
		c.drawRect(rectToDel, paint);

		if (dX < 0 && -dX <= view.getRight() >> 1 && Math.abs(dX) >= icon.getWidth())
			c.drawBitmap(icon, null,
			             new Rect(
					             view.getRight() + ((int) dX / 2) - icon.getWidth() / 2,
					             view.getBottom() - rectToDel.height() / 2 - icon.getHeight() / 2,
					             view.getRight() + ((int) dX / 2) + icon.getWidth() / 2,
					             view.getBottom() - rectToDel.height() / 2 + icon.getHeight() / 2),
			             paint);

	}
}
