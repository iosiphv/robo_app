package com.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import com.rest.RetrofitHelper;
import com.view.R;
import com.view.databinding.ActivityClientsChoisingBinding;
import com.vm.clients.ClientsVM;

/**
 * Created by admin on 06.10.2017.
 * <p>
 * Activity to choicing the client
 **/
public class ActivityClientsChoicing
		extends Activity
		implements OnQueryTextListener {

	public final static String KEY = "CLIENT";
	private ClientsVM vm;
	private ActivityClientsChoisingBinding binding;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		vm = new ClientsVM(RetrofitHelper.getRetrofit(getApplicationContext()));
		binding = DataBindingUtil.setContentView(this, R.layout.activity_clients_choising);

		LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext());
		layout.setInitialPrefetchItemCount(50);
		binding.recViewClients.setLayoutManager(layout);
		binding.recViewClients.setItemAnimator(new DefaultItemAnimator());
		binding.recViewClients.setHasFixedSize(true);
		binding.recViewClients.setItemViewCacheSize(10);

		binding.setVm(vm);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuItem search = menu.findItem(R.id.toolbar_menu_search);
		SearchView searchView = (SearchView) search.getActionView();
		searchView.setOnQueryTextListener(this);
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Bundle extras = new Bundle();
		extras.putSerializable(KEY, vm.getClient(0));
		data.putExtras(extras);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		vm.getFilter().filter(newText);
		return true;
	}
}
