package com.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import androidx.navigation.NavController;
import androidx.navigation.ui.NavigationUI;

import com.view.R;
import com.view.databinding.ActivityMainBinding;

import static androidx.navigation.Navigation.findNavController;
import static androidx.navigation.ui.NavigationUI.setupActionBarWithNavController;
import static androidx.navigation.ui.NavigationUI.setupWithNavController;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = MainActivity.class.getName();

	private ActivityMainBinding binding;
	private NavController       navController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
		this.setSupportActionBar(binding.toolbar);

		navController = findNavController(this, R.id.content_frame);
		setupActionBarWithNavController(this, navController, binding.drawerLayout);
		setupWithNavController(binding.drawerList, navController);
	}

	@Override
	public boolean onSupportNavigateUp() {
		return navController.navigateUp();
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		return NavigationUI.onNavDestinationSelected(item, navController) || super.onOptionsItemSelected(item);
	}
}