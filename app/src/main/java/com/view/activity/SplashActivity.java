package com.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.rest.RetrofitHelper;

import androidx.navigation.NavController;

public class SplashActivity extends AppCompatActivity{
	private NavController navController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		RetrofitHelper.getRetrofit(getApplicationContext());

		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(i);
		finish();
	}


	@Override
	public boolean onSupportNavigateUp() {
		return navController.navigateUp();
	}
}
