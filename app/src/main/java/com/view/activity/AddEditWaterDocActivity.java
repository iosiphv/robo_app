package com.view.activity;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;

import com.view.R;
import com.view.databinding.ActivityAddEditWaterDocBinding;
import com.vm.adapter.AdapterWaterDocContent;
import com.vm.docs.water.WaterDocVM;

import dao.pojo.doc.WaterDoc;

import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static android.view.View.DRAWING_CACHE_QUALITY_HIGH;

public class AddEditWaterDocActivity extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setEnterTransition(new Explode());
		getWindow().setExitTransition(new Explode());

		postponeEnterTransition();

		ActivityAddEditWaterDocBinding bind = DataBindingUtil
				.setContentView(this, R.layout.activity_add_edit_water_doc);

		RecyclerView rc = bind.rvWaterdocContent;

		rc.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
		                                            LinearLayoutManager.VERTICAL,
		                                            false));
		rc.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
		                                               VERTICAL));
		rc.setItemAnimator(new DefaultItemAnimator());

		rc.setItemViewCacheSize(10);
		rc.setHasFixedSize(true);
		rc.setDrawingCacheEnabled(true);
		rc.setDrawingCacheQuality(DRAWING_CACHE_QUALITY_HIGH);


		WaterDocVM vm;
		WaterDoc doc = (WaterDoc) getIntent().getSerializableExtra("WATER_DOC");

		if (doc != null) {
			vm = ViewModelProviders.of(this, new ViewModelProvider.Factory() {
				@NonNull
				@Override
				public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
					return (T) new WaterDocVM(doc);
				}
			}).get(WaterDocVM.class);

			bind.setVm(vm);
			rc.setAdapter(vm.getAdapter());
			bind.executePendingBindings();
		}

		startPostponedEnterTransition();
	}


}
