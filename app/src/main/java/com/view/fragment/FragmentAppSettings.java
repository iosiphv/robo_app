package com.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.rest.RetrofitHelper;
import com.view.R;

import java.util.Objects;

public class FragmentAppSettings
		extends PreferenceFragmentCompat
		implements SharedPreferences.OnSharedPreferenceChangeListener {

	@Override
	public void onStop() {
		super.onStop();
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getAppCont());
		pref.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getAppCont());
		pref.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(getAppCont());
		pref.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if (Objects.equals(getAppCont().getString(R.string.rest_server).toLowerCase(),
		                   key.toLowerCase()))
			RetrofitHelper.reCreateRetrofit(getAppCont());
	}

	private Context getAppCont() {
		return getActivity().getApplicationContext();
	}
}
