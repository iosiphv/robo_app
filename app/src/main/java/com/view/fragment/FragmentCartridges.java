package com.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.view.R;
import com.view.databinding.FragmentCartridgesBinding;
import com.vm.docs.cartridges.CartridgesVM;
import com.vm.listeners.itemtouch.OnCartridgeItemTouchListener;
import com.vm.listeners.anim.OnChildAttachWithAnimListener;
import com.vm.listeners.anim.OnRCItemBeahaviorByScrollDirection;
import com.vm.listeners.anim.OnRCScrollListener;
import com.vm.listeners.itemtouch.OnRcItemClickListener;

import java.util.concurrent.atomic.AtomicLong;

import static android.widget.LinearLayout.VERTICAL;

public class FragmentCartridges extends Fragment
		implements OnRcItemClickListener, SwipeRefreshLayout.OnRefreshListener, OnRCItemBeahaviorByScrollDirection {
	private static final String TAG = "FragmentCartridges";
	private AtomicLong TRANSLATION_Y_START_VALUE = new AtomicLong(500);


	private CartridgesVM vm;
	private FragmentCartridgesBinding binding;
	private final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), VERTICAL, false);

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		vm = ViewModelProviders.of(getActivity()).get(CartridgesVM.class);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		binding = FragmentCartridgesBinding
				.inflate(inflater, container, false);

		binding.swipeToRefreshCartridges.setOnRefreshListener(this);

		RecyclerView rc = binding.rcCartridges;

		rc.addOnScrollListener(new OnRCScrollListener(this));

		rc.addOnChildAttachStateChangeListener(new OnChildAttachWithAnimListener(
				TRANSLATION_Y_START_VALUE));

		rc.setItemViewCacheSize(10);
		rc.setItemAnimator(new DefaultItemAnimator());
		rc.setLayoutManager(layoutManager);
		rc.addOnItemTouchListener(new OnCartridgeItemTouchListener(getContext(), this));
		rc.setHasFixedSize(true);

		binding.setVm(vm);
		binding.executePendingBindings();
		return binding.getRoot();
	}

	@Override
	public void onClick(View view, int pos) {
		Log.d(TAG, "onClick: pos:" + pos + " view id:" + view.getId());
	}

	@Override
	public void onRefresh() {
		vm.fillCartridges(this::stopRefreshing);
	}

	private void stopRefreshing() {
		binding.swipeToRefreshCartridges.setRefreshing(false);
	}


	@Override
	public void onScrolledToTop() {

	}

	@Override
	public void onScrolledToBottom() {

	}

	@Override
	public void onScrolledUp() {
		TRANSLATION_Y_START_VALUE.set(-500);

	}

	@Override
	public void onScrolledDown() {
		TRANSLATION_Y_START_VALUE.set(500);
	}
}
