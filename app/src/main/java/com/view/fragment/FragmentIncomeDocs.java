package com.view.fragment;

import android.app.ActivityOptions;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.Navigation;

import com.view.R;
import com.view.databinding.FragmentIncomeDocBinding;
import com.view.helper.RCIncomeDocHeadItemSwipe;
import com.view.helper.RCIncomeDocHeadItemSwipe.OnSwipedListener;
import com.vm.docs.income.IncomeDocsVM;
import com.vm.listeners.anim.OnChildAttachWithAnimListener;
import com.vm.listeners.anim.OnRCItemBeahaviorByScrollDirection;
import com.vm.listeners.anim.OnRCScrollListener;
import com.vm.listeners.itemtouch.OnIncomeDocTouchListener;
import com.vm.listeners.itemtouch.OnRcItemClickListener;

import java.util.concurrent.atomic.AtomicLong;

import dao.pojo.doc.IncomeDoc;

import static android.support.v4.view.ViewCompat.getTransitionName;
import static android.support.v7.widget.SearchView.OnQueryTextListener;

public class FragmentIncomeDocs
		extends Fragment
		implements OnRefreshListener, OnQueryTextListener,
		OnRcItemClickListener, OnSwipedListener, OnRCItemBeahaviorByScrollDirection {

	private final String TAG = "FragmentIncomeDocs";

	private OnIncomeDocTouchListener incomeDocTouchListener;
	private ItemTouchHelper          itemTouchHelper;
	private FragmentIncomeDocBinding binding;
	private IncomeDocsVM             vm;
	private SearchView               mSearchView;
	private LinearLayoutManager      linearLMRecViewHead;
	private AtomicLong               starVal = new AtomicLong(500);

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		vm = ViewModelProviders.of(this).get(IncomeDocsVM.class);

		itemTouchHelper = new ItemTouchHelper(new RCIncomeDocHeadItemSwipe(this));
		incomeDocTouchListener = new OnIncomeDocTouchListener(getContext(), this);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState
	) {
		super.onCreateView(inflater, container, savedInstanceState);
		Log.d(TAG, "onCreateView");

		binding = FragmentIncomeDocBinding.inflate(inflater, container, false);
		binding.setIncomeDocsVM(vm);
		binding.executePendingBindings();

		binding.swipteToRefreshMainDocs.setOnRefreshListener(this);

		RecyclerView rcView = binding.recyclerViewHead;

		//swipe to left, swipe to right
		itemTouchHelper.attachToRecyclerView(rcView);

		linearLMRecViewHead = new LinearLayoutManager(getContext());
		linearLMRecViewHead.setInitialPrefetchItemCount(10);

		rcView.addOnChildAttachStateChangeListener(new OnChildAttachWithAnimListener(starVal));
		rcView.addOnScrollListener(new OnRCScrollListener(this));

		rcView.addOnItemTouchListener(incomeDocTouchListener);
		rcView.setLayoutManager(linearLMRecViewHead);
		rcView.setItemAnimator(new DefaultItemAnimator());
		rcView.setHasFixedSize(true);
		rcView.setNestedScrollingEnabled(false);
		rcView.setDrawingCacheEnabled(true);
		rcView.setItemViewCacheSize(25);

		return binding.getRoot();
	}

	@Override
	public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
		/*
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
		Activity activity = getActivity();
		Log.d(TAG, "onCreateOptionsMenu");
		inflater.inflate(R.menu.menu_search, menu);

		SearchManager sMGR = (SearchManager) activity.getSystemService(Context.SEARCH_SERVICE);

		mSearchView = (SearchView) menu.findItem(R.id.toolbar_menu_search).getActionView();
		mSearchView.setSearchableInfo(sMGR.getSearchableInfo(activity.getComponentName()));
		mSearchView.setIconifiedByDefault(false);
		mSearchView.setSubmitButtonEnabled(false);

		mSearchView.setOnQueryTextListener(this);
		*/
	}

	@Override
	public void onRefresh() {
		showSnackbar(R.string.process_geting_docs);

		vm.clearData();

		vm.fillDocsFromREST(() -> {
			stopRefreshing();
			if (vm.getDocsCount() > 0)
				binding.recyclerViewHead.scrollToPosition(vm.getDocsCount() - 1);
			showSnackbar(R.string.process_geting_docs_end);
		});

	}

	private void showSnackbar(int resString) {
		Snackbar.make(binding.swipteToRefreshMainDocs,
				resString, Snackbar.LENGTH_SHORT)
		        .show();
	}

	private void showSnackbar(String msg) {
		Snackbar.make(binding.swipteToRefreshMainDocs,
				msg, Snackbar.LENGTH_SHORT)
		        .show();
	}

	private void stopRefreshing() {
		binding.swipteToRefreshMainDocs.setRefreshing(false);
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		vm.filter(query);
		mSearchView.clearFocus();
		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}

	private void onHeadClick(CardView v, int pos) {
		IncomeDoc doc = vm.getDocByPosition(pos);
		Log.d(TAG, "onHeadClick: " + doc);

		if (doc.getContentDocList().size() < 1) return;

		Pair pair[]            = new Pair[1];
		View sharedElementView = v.findViewById(R.id.card_view_head_doc);
		pair[0] = Pair.create(sharedElementView, getTransitionName(sharedElementView));
		ActivityOptions optionsCompat = ActivityOptions
				.makeSceneTransitionAnimation(getActivity(), pair);

		Bundle b = optionsCompat.toBundle();
		b.putSerializable("HEAD_DOC", doc);

		Navigation.findNavController(v).navigate(R.id.action_toEditDocFragment, b);
	}

	@Override
	public void onSwiped(int direction, int pos) {
		if (direction == ItemTouchHelper.START) {
			showSnackbar(R.string.process_delete_doc);
			if (vm.delDoc(pos)) showSnackbar(R.string.process_delete_doc_end);
		}
/*
		if (direction == ItemTouchHelper.END) {
			Intent intent = new Intent(getContext(), EditDocFragment.class);
			intent.putExtra("HEAD_DOC", vm.getDocByPosition(pos));
			getContext().startActivity(intent);
		}
		*/
	}

	@Override
	public void onClick(View view, int pos) {
		onHeadClick((CardView) view, pos);
	}

	@Override
	public void onScrolledToTop() {
	}

	@Override
	public void onScrolledToBottom() {
	}

	@Override
	public void onScrolledUp() {
		starVal.set(-500);
	}

	@Override
	public void onScrolledDown() {
		starVal.set(500);
	}
}
