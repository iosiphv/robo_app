package com.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.transition.Explode;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.view.R;
import com.view.activity.AddEditWaterDocActivity;
import com.view.databinding.FragmentWaterDocBinding;
import com.view.helper.RCWaterDocSwipeToDel;
import com.vm.docs.water.WaterDocsVM;
import com.vm.listeners.anim.OnChildAttachWithAnimListener;
import com.vm.listeners.anim.OnRCItemBeahaviorByScrollDirection;
import com.vm.listeners.anim.OnRCScrollListener;
import com.vm.listeners.itemtouch.OnRcItemClickListener;
import com.vm.listeners.itemtouch.OnWaterDocTouchListener;

import java.util.concurrent.atomic.AtomicLong;

import dao.pojo.doc.WaterDoc;

import static android.support.v4.view.ViewCompat.getTransitionName;
import static android.support.v7.widget.DividerItemDecoration.VERTICAL;
import static android.view.View.DRAWING_CACHE_QUALITY_HIGH;

public class FragmentWaterDocs extends Fragment
		implements OnRefreshListener, OnRCItemBeahaviorByScrollDirection, OnRcItemClickListener {

	private String TAG = FragmentWaterDocs.class.getCanonicalName();
	private FragmentWaterDocBinding binding;
	private WaterDocsVM viewModel;
	private AtomicLong startVal = new AtomicLong(500);

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		viewModel = ViewModelProviders
				.of(getActivity())
				.get(WaterDocsVM.class);
		setEnterTransition(new Explode());
		setExitTransition(new Explode());
		setReenterTransition(new Explode());
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		Log.d(TAG, "onCreateView " + this.toString());

		binding = FragmentWaterDocBinding.inflate(inflater, container, false);
		binding.setViewModel(viewModel);
		binding.executePendingBindings();

		binding.swipeToRefreshWaterDocs.setOnRefreshListener(this);

		RecyclerView waterList = binding.rcWaterList;

		waterList.setAdapter(viewModel.getAdapter());

		LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
		layoutManager.setInitialPrefetchItemCount(10);

		waterList.setLayoutManager(layoutManager);
		waterList.addItemDecoration(new DividerItemDecoration(getContext(), VERTICAL));

		ItemTouchHelper ith = new ItemTouchHelper(new RCWaterDocSwipeToDel(viewModel));
		ith.attachToRecyclerView(waterList);

		waterList.addOnItemTouchListener(new OnWaterDocTouchListener(getContext(), this));

		waterList.addOnChildAttachStateChangeListener(new OnChildAttachWithAnimListener(startVal));
		waterList.addOnScrollListener(new OnRCScrollListener(this));

		waterList.setItemViewCacheSize(10);
		waterList.setItemAnimator(new DefaultItemAnimator());
		waterList.setHasFixedSize(true);
		waterList.setDrawingCacheEnabled(true);
		waterList.setDrawingCacheQuality(DRAWING_CACHE_QUALITY_HIGH);

		return binding.getRoot();
	}

	@Override
	public void onRefresh() {
		final SwipeRefreshLayout view = binding.swipeToRefreshWaterDocs;

		showSnackbar(R.string.process_geting_docs);

		viewModel.fillDocsFromREST(() -> {
			view.setRefreshing(false);
			if (viewModel.getItemsCount() > 0)
				binding.rcWaterList.scrollToPosition(viewModel.getItemsCount() - 1);
			showSnackbar(R.string.process_geting_docs_end);
		});
	}

	private void showSnackbar(int resString) {
		Snackbar.make(binding.swipeToRefreshWaterDocs,
		              resString, Snackbar.LENGTH_SHORT)
		        .show();
	}

	private void showSnackbar(String msg) {
		Snackbar.make(binding.swipeToRefreshWaterDocs,
		              msg, Snackbar.LENGTH_SHORT)
		        .show();
	}

	@Override
	public void onScrolledToTop() {

	}

	@Override
	public void onScrolledToBottom() {

	}

	@Override
	public void onScrolledUp() {
		startVal.set(-500);

	}

	@Override
	public void onScrolledDown() {
		startVal.set(500);
	}

	@Override
	public void onClick(View view, int pos) {
		WaterDoc doc = viewModel.getDocByPosition(pos);

		Pair[] pairs = new Pair[4];

		View viewShared;

		viewShared = view.findViewById(R.id.tv_str_sum);
		pairs[0] = Pair.create(viewShared, getTransitionName(viewShared));

		viewShared = view.findViewById(R.id.tv_sum);
		pairs[1] = Pair.create(viewShared, getTransitionName(viewShared));

		viewShared = view.findViewById(R.id.tv1);
		pairs[2] = Pair.create(viewShared, getTransitionName(viewShared));

		viewShared = view.findViewById(R.id.tv_date_cont);
		pairs[3] = Pair.create(viewShared, getTransitionName(viewShared));

		ActivityOptionsCompat optionsCompat = ActivityOptionsCompat
				.makeSceneTransitionAnimation(getActivity(), pairs);

		Intent intent = new Intent(getContext(), AddEditWaterDocActivity.class);
		intent.putExtra("WATER_DOC", doc);

		startActivity(intent, optionsCompat.toBundle());
	}
}
