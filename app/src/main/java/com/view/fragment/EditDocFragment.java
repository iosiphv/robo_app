package com.view.fragment;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.view.databinding.FragmentAddIncomeDocBinding;
import com.vm.docs.income.IncomeDocVM;

import dao.pojo.doc.IncomeDoc;

public class EditDocFragment extends Fragment {
	private FragmentAddIncomeDocBinding binding;
	private static final String TAG = EditDocFragment.class.getSimpleName();

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		binding = FragmentAddIncomeDocBinding.inflate(inflater, container, false);
		IncomeDoc incomeDoc = (IncomeDoc) getArguments().getSerializable("HEAD_DOC");

		Log.d(TAG, "onCreate: pref HEAD_DOC " + incomeDoc);

		IncomeDocVM viewModel = ViewModelProviders
				.of(this, new IncomeDocFactory(incomeDoc))
				.get(IncomeDocVM.class);

		RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
		RecyclerView rc = binding.recyclerViewContent;
		rc.setLayoutManager(mLayoutManager);
		rc.setItemAnimator(new DefaultItemAnimator());
		rc.setHasFixedSize(true);
		rc.setItemViewCacheSize(5);
		rc.setDrawingCacheEnabled(true);
		rc.setAdapter(viewModel.getAdapterContent());

		binding.setVm(viewModel);
		binding.executePendingBindings();

		return binding.getRoot();
	}

	private class IncomeDocFactory implements ViewModelProvider.Factory {
		private IncomeDoc doc;

		IncomeDocFactory(IncomeDoc doc) {
			this.doc = doc;
		}

		@NonNull
		@Override
		public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
			return (T) new IncomeDocVM(doc);
		}
	}

}