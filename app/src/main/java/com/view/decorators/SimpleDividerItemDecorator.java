package com.view.decorators;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ovt10 on 14.04.2017.
 */
public class SimpleDividerItemDecorator extends RecyclerView.ItemDecoration {
	private Drawable divider;

	public SimpleDividerItemDecorator(Drawable divider) {
		this.divider = divider;
	}

	@Override
	public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
		int offset = 0;
		final int left = parent.getPaddingLeft() + offset;
		final int right = parent.getWidth() - parent.getPaddingRight() - offset;

		final int childCount = parent.getChildCount();
		for (int i = 0; i < childCount; i++) {
			final View child = parent.getChildAt(i);
			final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

			final int top = child.getBottom() + params.bottomMargin;
			final int bottom = top + divider.getIntrinsicHeight();

			divider.setBounds(left, top, right, bottom);
			divider.draw(c);
		}
	}
}
