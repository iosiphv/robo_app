package com.rest.income;

import java.util.ArrayList;

import dao.pojo.doc.IncomeDoc;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface IncomeDocsService {
	@Headers({
			"Accept: application/json",
			"Content-Type: application/json"
	})

	@GET("income/list/byCurrentMonth")
	Call<ArrayList<IncomeDoc>> getDocsByCurrMonth();

	@PUT("income/list/byCurrentMonth")
	Call<ArrayList<IncomeDoc>> insertDoc(@Body IncomeDoc doc);

	@DELETE("income/{koddoc}")
	Call<IncomeDoc> deleteDoc(@Path("koddoc") int koddoc);
}
