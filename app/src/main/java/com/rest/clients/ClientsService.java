package com.rest.clients;

import java.util.ArrayList;

import dao.pojo.Client;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by admin on 06.10.2017.
 */

public interface ClientsService {
	@Headers({
			"Accept: application/json",
			"Content-Type: application/json"
	})

	@GET("utils/clients")
	Call<ArrayList<Client>> getClients();
}
