package com.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.view.R;

import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient.Builder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class RetrofitHelper {

	private static final String TAG = "RetrofitHelper";
	private static String srvAdress;

	private static Retrofit retrofit = null;
	private Context appContext;

	private RetrofitHelper(Context appContext) {
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		this.appContext = appContext;

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(appContext);
		srvAdress = prefs
				.getString(appContext.getString(R.string.rest_server),
				           "http://172.26.1.73:8080/");

		retrofit = new Retrofit.Builder()
				.baseUrl(srvAdress)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.client(createBuilder().build())
				.build();

		Log.d(TAG, "RetrofitHelper: Created...");
	}

	public static String getSrvAdress() {
		return srvAdress;
	}

	@Deprecated
	public synchronized static Retrofit getRetrofit() {
		if (retrofit == null)
			throw new UnsupportedOperationException("Using uninitialized RetrofitHelper");
		return retrofit;
	}

	public synchronized static Retrofit getRetrofit(Context appContext) {
		Log.d(TAG, "-==GET RETROFIT==-");
		if (retrofit == null) new RetrofitHelper(appContext);
		return retrofit;
	}

	public synchronized static void reCreateRetrofit(Context appContext) {
		retrofit = null;
		new RetrofitHelper(appContext);
	}

	private Builder createBuilder() {
		Builder builder = new Builder();
		builder.connectTimeout(10, TimeUnit.SECONDS);
		builder.readTimeout(10, TimeUnit.SECONDS);
		builder.writeTimeout(10, TimeUnit.SECONDS);

		builder.cache(new Cache(appContext.getCacheDir(), 100 * 1024 * 1024));

		return builder;
	}
}