package com.rest.water;

import java.util.ArrayList;

import dao.pojo.doc.WaterDoc;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by ovt10 on 11.01.2017.
 */
public interface WaterDocsService {
	@Headers({
			"Accept: application/json",
			"Content-Type: application/json"
	})

	@GET("waterdoc")
	Call<ArrayList<WaterDoc>> getAllDocs();

	@GET("waterdoc/insert/new")
	Call<WaterDoc> createNewDoc();

	@POST("waterdoc/{koddoc}/pay/{kodm}")
	Call<Void> performClientPay(@Path("koddoc") int koddoc,
	                            @Path("kodm") int kodM,
	                            @Body double sumPay);
}
