package com.vm.clients;

import android.widget.Filter;

import com.model.dao.rest.DAORESTClientsChoisingHelper;
import com.rest.clients.ClientsService;
import com.vm.adapter.AdapterClientsList;

import java.util.ArrayList;

import dao.pojo.Client;
import retrofit2.Retrofit;

/**
 * Created by admin on 06.10.2017.
 */

public class ClientsVM {
	private final String TAG = ClientsVM.class.getName();
	private final AdapterClientsList adapter;
	private ArrayList<Client> clients;
	private ArrayList<Client> clientsFiltered = new ArrayList<>(50);

	public ClientsVM(Retrofit retrofit) {
		adapter = new AdapterClientsList(this);
		ClientsService clService = retrofit.create(ClientsService.class);
		DAORESTClientsChoisingHelper helper = new DAORESTClientsChoisingHelper(clService);

		clients = helper.getDocs(list -> getAdapter().notifyDataSetChanged());
	}

	public AdapterClientsList getAdapter() {
		return adapter;
	}

	public Client getClient(int position) {
		return clients.get(position);
	}

	public int getCount() {
		return clients.size();
	}

	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				String searchStr = charSequence.toString();

				if (searchStr.isEmpty()) clientsFiltered = clients;
				else
					for (Client cl : clients)
						if (cl.getName().contains(searchStr))
							clientsFiltered.add(cl);

				FilterResults filterResults = new FilterResults();
				filterResults.count = clientsFiltered.size();
				filterResults.values = clientsFiltered;
				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
				clients = (ArrayList<Client>) filterResults.values;
				getAdapter().notifyDataSetChanged();
			}
		};
	}
}
