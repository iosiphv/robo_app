package com.vm.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.view.R;
import com.view.databinding.RcRowActivityClientsChoicingBinding;
import com.vm.clients.ClientsVM;

import dao.pojo.Client;

/**
 * Created by admin on 09.10.2017.
 */

public class AdapterClientsList
		extends RecyclerView.Adapter<AdapterClientsList.ClientViewHolder>
		implements Filterable {

	private ClientsVM vm;
	private String currentGroup = "Администрация";/*Start group*/

	public AdapterClientsList(final ClientsVM vm) {
		this.vm = vm;
	}

	@Override
	public ClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View v = inflater.inflate(R.layout.rc_row_activity_clients_choicing, parent, false);
		return new ClientViewHolder(v);
	}

	@Override
	public void onBindViewHolder(ClientViewHolder holder, int position) {
		Client client = vm.getClient(position);
		if (currentGroup.equals(client.getGroupName())) {
			holder.rowBinding.tvSectionName.setVisibility(View.GONE);
		} else {
			holder.rowBinding.tvSectionName.setVisibility(View.VISIBLE);
			currentGroup = client.getGroupName();
		}

		holder.rowBinding.setClient(client);
		holder.rowBinding.executePendingBindings();
	}

	@Override
	public int getItemCount() {
		return vm.getCount();
	}

	@Override
	public Filter getFilter() {
		return vm.getFilter();
	}

	public class ClientViewHolder extends RecyclerView.ViewHolder
			implements View.OnClickListener {
		RcRowActivityClientsChoicingBinding rowBinding;

		public ClientViewHolder(View v) {
			super(v);
			rowBinding = RcRowActivityClientsChoicingBinding.bind(v);
		}

		@Override
		public void onClick(View view) {

		}
	}
}
