package com.vm.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.view.R;
import com.view.databinding.RcRowWaterDocsContentBinding;

import java.util.List;

import dao.pojo.doc.ContentWaterDoc;

public class AdapterWaterDocContent
		extends RecyclerView.Adapter<AdapterWaterDocContent.WaterDocContentViewHolder> {
	private List<ContentWaterDoc> contentDocList;

	public AdapterWaterDocContent(List<ContentWaterDoc> contentDocList) {
		this.contentDocList = contentDocList;
	}

	@Override
	public WaterDocContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		RcRowWaterDocsContentBinding bind =
				DataBindingUtil
						.inflate(LayoutInflater.from(parent.getContext()),
						         R.layout.rc_row_water_docs_content,
						         parent, false);
		return new WaterDocContentViewHolder(bind);
	}

	@Override
	public void onBindViewHolder(final WaterDocContentViewHolder holder, final int position) {
		holder.setContDoc(contentDocList.get(position));
	}

	@Override
	public int getItemCount() {
		return contentDocList.size();
	}

	class WaterDocContentViewHolder
			extends RecyclerView.ViewHolder {
		private RcRowWaterDocsContentBinding binding;

		WaterDocContentViewHolder(RcRowWaterDocsContentBinding bind) {
			super(bind.getRoot());
			binding = bind;
		}

		void setContDoc(ContentWaterDoc contDoc) {
			binding.setContDoc(contDoc);
			binding.executePendingBindings();
		}
	}
}
