package com.vm.adapter.filter;

import android.widget.Filter;

import com.vm.docs.income.IncomeDocsVM;

import java.util.ArrayList;

import dao.pojo.doc.ContentDoc;
import dao.pojo.doc.IncomeDoc;

public class FilterIncomeDocs extends Filter {
	private final IncomeDocsVM viewModel;

	public FilterIncomeDocs(final IncomeDocsVM viewModel) {
		super();
		this.viewModel = viewModel;
	}

	@Override
	protected FilterResults performFiltering(CharSequence sStr) {
		FilterResults results = new FilterResults();
		ArrayList<IncomeDoc> filteredList = new ArrayList<>(5);

		final CharSequence finalSStr = sStr.toString().toLowerCase();

		for (IncomeDoc doc : viewModel.getItems()) {
			boolean isAdd = false;
			if (doc.getIncome().getName().toLowerCase().contains(finalSStr)) isAdd = true;
			if (!isAdd && doc.getOutgone().getName().toLowerCase().contains(finalSStr)) isAdd = true;

			if (!isAdd)
				for (ContentDoc c : doc.getContentDocList()) {
					if (!isAdd && c.getKodM().getClient().getName().toLowerCase().contains(finalSStr))
						isAdd = true;
					if (!isAdd && c.getKodM().getInner().toLowerCase().contains(finalSStr)) isAdd = true;
					if (!isAdd && c.getKodM().getSerial().toLowerCase().contains(finalSStr)) isAdd = true;
					if (!isAdd && c.getKodM().getInv().toLowerCase().contains(finalSStr)) isAdd = true;
				}

			if (isAdd) filteredList.add(doc);
		}

		results.count = filteredList.size();
		results.values = filteredList;
		return results;
	}

	@Override
	protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
		viewModel.clearData();
		viewModel.addDocs((ArrayList<IncomeDoc>) filterResults.values);
	}
}
