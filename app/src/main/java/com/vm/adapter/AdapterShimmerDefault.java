package com.vm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

public class AdapterShimmerDefault extends RecyclerView.Adapter<AdapterShimmerDefault.ShimmerVH> {
	@NonNull
	@Override
	public ShimmerVH onCreateViewHolder(@NonNull ViewGroup parent,
	                                    int viewType) {
//		Context context = parent.getContext();
//		RcRowShimmerBinding binding =
//		DataBindingUtil.inflate(LayoutInflater.from(context),
//		                        R.layout.rc_row_shimmer,
//		                        parent,
//		                        false);
//		return new ShimmerVH(binding.getRoot());
		return null;
	}

	@Override
	public void onBindViewHolder(@NonNull ShimmerVH holder, int position) {

	}

	@Override
	public int getItemCount() {
		return 0;
	}

	public class ShimmerVH extends RecyclerView.ViewHolder {

		public ShimmerVH(View itemView) {
			super(itemView);
		}
	}

}
