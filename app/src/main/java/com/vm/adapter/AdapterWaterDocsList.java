package com.vm.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.view.R;
import com.view.databinding.RcRowWaterDocsListBinding;
import com.vm.docs.water.WaterDocsVM;

import dao.pojo.doc.ContentWaterDoc;
import dao.pojo.doc.WaterDoc;

import static android.view.LayoutInflater.from;

public class AdapterWaterDocsList
		extends RecyclerView.Adapter<AdapterWaterDocsList.WaterDocViewHolder> {
	private final WaterDocsVM viewModel;

	public AdapterWaterDocsList(WaterDocsVM viewModel) {
		this.viewModel = viewModel;
	}

	@Override
	public WaterDocViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		RcRowWaterDocsListBinding bind = DataBindingUtil
				.inflate(from(parent.getContext()),
				         R.layout.rc_row_water_docs_list,
				         parent, false);
		return new WaterDocViewHolder(bind);
	}

	@Override
	public void onBindViewHolder(WaterDocViewHolder holder, int position) {
		holder.setViewModel(viewModel.getDocByPosition(position));
	}

	@Override
	public int getItemCount() {
		return viewModel.getItemsCount();
	}

	class WaterDocViewHolder extends RecyclerView.ViewHolder {
		private RcRowWaterDocsListBinding binding;

		WaterDocViewHolder(RcRowWaterDocsListBinding bind) {
			super(bind.getRoot());
			binding = bind;
		}

		void setViewModel(WaterDoc doc) {
			Double sumOverDoc = 0d;
			for (ContentWaterDoc c : doc.getContentDocList())	sumOverDoc += c.getQuant();
			binding.setDoc(doc);
			binding.setSumGiven(sumOverDoc);
			binding.executePendingBindings();
		}
	}
}
