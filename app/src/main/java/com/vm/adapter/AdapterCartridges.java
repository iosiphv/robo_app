package com.vm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.view.databinding.RcRowCartridgesBinding;
import com.vm.docs.cartridges.CartridgesVM;

import dao.pojo.KodM;

public class AdapterCartridges extends RecyclerView.Adapter<AdapterCartridges.CartridgeVH> {

	private CartridgesVM vm;

	public AdapterCartridges(CartridgesVM vm) {
		this.vm = vm;
	}

	@NonNull
	@Override
	public CartridgeVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());

		RcRowCartridgesBinding bind = RcRowCartridgesBinding
				.inflate(inflater, parent, false);

		return new CartridgeVH(bind);
	}

	@Override
	public void onBindViewHolder(@NonNull CartridgeVH holder, int position) {
		holder.setKodM(vm.getKodM(position));
	}

	@Override
	public int getItemCount() {
		return vm.getItemsCount();
	}

	class CartridgeVH extends RecyclerView.ViewHolder {
		private RcRowCartridgesBinding binding;

		CartridgeVH(RcRowCartridgesBinding binding) {
			super(binding.getRoot());
			this.binding = binding;
		}

		void setKodM(KodM kodm) {
			binding.setObjKodM(kodm);
			binding.executePendingBindings();
		}
	}
}
