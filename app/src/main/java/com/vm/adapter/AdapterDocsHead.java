package com.vm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.view.databinding.RcRowIncomeDocHeadBinding;
import com.vm.docs.income.IncomeDocsVM;

import dao.pojo.doc.IncomeDoc;

public class AdapterDocsHead extends RecyclerView.Adapter<AdapterDocsHead.IncomeDocViewHolder> {
	private final IncomeDocsVM viewModel;
	private static final String TAG = "AdapterDocsHead";

	public AdapterDocsHead(final IncomeDocsVM incomeDocsVM) {
		viewModel = incomeDocsVM;
	}

	@Override
	public IncomeDocViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater lI = LayoutInflater.from(parent.getContext());

		RcRowIncomeDocHeadBinding binding = RcRowIncomeDocHeadBinding.inflate(lI, parent, false);

		Log.d(TAG, "onCreateViewHolder:" + binding.hashCode());
		return new IncomeDocViewHolder(binding);
	}

	@Override
	public void onViewAttachedToWindow(@NonNull IncomeDocViewHolder holder) {
		super.onViewAttachedToWindow(holder);
		holder.onAttach();
	}

	@Override
	public void onViewDetachedFromWindow(@NonNull IncomeDocViewHolder holder) {
		super.onViewDetachedFromWindow(holder);
		holder.onDettach();
	}

	@Override
	public void onBindViewHolder(IncomeDocViewHolder holder, int position) {
		holder.setDoc(viewModel.getDocByPosition(position));
	}

	@Override
	public int getItemCount() {
		return viewModel.getDocsCount();
	}

	class IncomeDocViewHolder extends RecyclerView.ViewHolder {
		private RcRowIncomeDocHeadBinding binding;
		private IncomeDoc doc;

		IncomeDocViewHolder(RcRowIncomeDocHeadBinding bind) {
			super(bind.getRoot());
			this.binding = bind;
		}

		void onAttach() {
			binding.setDoc(doc);
			binding.executePendingBindings();
		}

		void onDettach() {
			binding.invalidateAll();
		}

		public void setDoc(IncomeDoc doc) {
			this.doc = doc;
		}
	}
}
