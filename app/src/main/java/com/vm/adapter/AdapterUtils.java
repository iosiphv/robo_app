package com.vm.adapter;

import com.vm.listeners.OnRESTWorkDoneListener;

import java.sql.Date;
import java.util.ArrayList;

import dao.pojo.Client;
import dao.pojo.KodM;
import dao.pojo.doc.ContentDoc;
import dao.pojo.doc.IncomeDoc;

import static dao.pojo.doc.Doc.TYPE_TRANSFER.MOVEMENT;

/**
 * Created by ovt10 on 11.08.2016.
 */
public class AdapterUtils {
	public static ArrayList<IncomeDoc> createAdapterData(OnRESTWorkDoneListener listener) {
		ArrayList<IncomeDoc> ret = new ArrayList<>(10);

		for (int i = 0; i < 50; i++) {
			IncomeDoc doc = new IncomeDoc();

			Client clIn = new Client();
			clIn.setName("Бухгалтерия");

			Client clOut = new Client();
			clOut.setName("Демиденко Сергій Віталійович Фізична особа - підприемець");

			Client clKodm = new Client();
			clKodm.setName("Canon 7" + i % 100);

			KodM kodm = new KodM();
			kodm.setClient(clKodm);
			kodm.setInner("765");
			kodm.setSerial(String.valueOf(i));

			ContentDoc contentDoc = new ContentDoc();
			contentDoc.setKodM(kodm);
			contentDoc.setQuant(1);
			contentDoc.setComment(" CoMMENT TESTING DATA");

			ArrayList<ContentDoc> cDocList = new ArrayList<>(1);
			for (int j = 0; j < i % 25; j++) cDocList.add(contentDoc);

			doc.setDate(new Date(System.currentTimeMillis()));
			doc.setNum((int) (System.currentTimeMillis() + i));
			doc.setIncome(clIn);
			doc.setOutgone(clOut);
			doc.setTypeOfTransfer(MOVEMENT);
			doc.setContentDocList(cDocList);

			ret.add(doc);
		}
		listener.onRestWorkDone(ret);
		return ret;
	}
}
