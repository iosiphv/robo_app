package com.vm.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.view.databinding.RcRowIncomeDocContentBinding;

import java.util.List;

import dao.pojo.doc.ContentDoc;

public class AdapterDocsContent extends RecyclerView.Adapter<AdapterDocsContent.ContentViewHolder> {
	private List<ContentDoc> docs;

	public AdapterDocsContent(List<ContentDoc> list) {
		this.docs = list;
	}

	@Override
	public ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		RcRowIncomeDocContentBinding binding = RcRowIncomeDocContentBinding
				.inflate(inflater,
				         parent,
				         false);
		return new ContentViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(ContentViewHolder holder, int position) {
		holder.setContentDoc(docs.get(position));
	}

	@Override
	public int getItemCount() {
		return docs.size();
	}

	public void addDoc(final ContentDoc doc) {
		this.docs.add(doc);
		notifyItemInserted(docs.size());
	}

	public void addDocs(final List<ContentDoc> docs) {
		Log.d("INCOMEDOCCONT", "addDocs: " + docs.size());
		int i = this.docs.size();
		this.docs.addAll(docs);
		notifyItemRangeInserted(i, docs.size());
	}

	public void clearDocs() {
		int i = docs.size();
		docs.clear();
		notifyItemRangeRemoved(0, i);
	}

	class ContentViewHolder extends RecyclerView.ViewHolder {
		private final RcRowIncomeDocContentBinding binding;

		ContentViewHolder(RcRowIncomeDocContentBinding b) {
			super(b.getRoot());
			binding = b;
		}

		void setContentDoc(ContentDoc cd) {
			binding.setDoc(cd);
			binding.executePendingBindings();
		}
	}
}
