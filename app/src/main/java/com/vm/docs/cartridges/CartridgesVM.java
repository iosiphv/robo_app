package com.vm.docs.cartridges;

import android.arch.lifecycle.ViewModel;

import com.vm.adapter.AdapterCartridges;
import com.vm.listeners.OnVMWorkDoneListener;

import java.util.ArrayList;

import dao.pojo.Client;
import dao.pojo.KodM;

public class CartridgesVM extends ViewModel {


	private ArrayList<KodM> cartridges = new ArrayList<>(10);

	private AdapterCartridges adapter = new AdapterCartridges(this);

	public KodM getKodM(int position) {
		return cartridges.get(position);
	}

	public int getItemsCount() {
		return cartridges.size() - 1;
	}

	public AdapterCartridges getAdapter() {
		return adapter;
	}

	public void fillCartridges(OnVMWorkDoneListener listener) {
		ArrayList<KodM> data = getTempData();
		addCartridges(data);

		listener.onWorkDone();
	}

	private void addCartridges(ArrayList<KodM> data) {
		cartridges.addAll(data);
		adapter.notifyItemRangeInserted(cartridges.size() - 1, data.size() - 1);
	}

	private ArrayList<KodM> getTempData() {
		ArrayList<KodM> items = new ArrayList<>(5);

		for (int i = 0; i < 150; i++) {
			Client cl = new Client(1);
			cl.setName("werieuoirwueriouwe" + i);

			KodM kodM = new KodM(cl);
			kodM.setInner(String.valueOf(i));
			kodM.setSerial("12asfdaksjfklasjf3qweqwe" + i);
			kodM.setInv(String.valueOf(i >> 4));

			items.add(kodM);
		}


		return items;
	}
}
