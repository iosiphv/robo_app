package com.vm.docs.water;

import android.arch.lifecycle.ViewModel;

import com.model.dao.rest.DAORESTWaterDocsHelper;
import com.vm.adapter.AdapterWaterDocContent;

import dao.pojo.doc.WaterDoc;

public class WaterDocVM extends ViewModel {
	private final DAORESTWaterDocsHelper docsHelper = new DAORESTWaterDocsHelper();
	private final WaterDoc doc;
	private final AdapterWaterDocContent adapter;

	public WaterDocVM(WaterDoc doc) {
		this.doc = doc;

		this.adapter = new AdapterWaterDocContent(doc.getContentDocList());
	}

	public AdapterWaterDocContent getAdapter() {return adapter;}

	public WaterDoc getDoc() {return doc;}
}
