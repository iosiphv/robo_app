package com.vm.docs.water;

import android.arch.lifecycle.ViewModel;

import com.model.dao.rest.DAORESTWaterDocsHelper;
import com.vm.adapter.AdapterWaterDocsList;
import com.vm.listeners.OnVMWorkDoneListener;

import java.util.ArrayList;

import dao.pojo.doc.WaterDoc;

public class WaterDocsVM extends ViewModel {
	private static final String DEBUG_TAG = "WaterDocsVM";
	private final DAORESTWaterDocsHelper docsHelper = new DAORESTWaterDocsHelper();
	private ArrayList<WaterDoc> items = new ArrayList<>(3);
	private AdapterWaterDocsList adapter = new AdapterWaterDocsList(this);

	public AdapterWaterDocsList getAdapter() {
		return adapter;
	}

	private void addDocs(final ArrayList<WaterDoc> docs) {
		items.addAll(docs);
		adapter.notifyItemRangeInserted(items.size(), docs.size());
	}

	private void addDoc(final WaterDoc doc) {
		items.add(doc);
		adapter.notifyItemInserted(items.size() + 1);
	}

	public boolean deleteDoc(int pos) {
		boolean rv = false;
		if (items.remove(pos) != null) {
			getAdapter().notifyItemRemoved(pos);
			rv = true;
		}
		return rv;
	}

	private void clear() {
		int i = items.size();
		items.clear();
		adapter.notifyItemRangeRemoved(0, i);
	}

	public int getItemsCount() {
		return items.size();
	}

	public WaterDoc getDocByPosition(int position) {
		return items.get(position);
	}

	public void clientMakePayment(int koddoc, int kodM, double sumPay) {
		docsHelper.clientMakePayment(koddoc, kodM, sumPay,
		                             list -> {});
	}

	public void fillDocsFromREST(OnVMWorkDoneListener l) {
		clear();
		docsHelper.getDocs(list -> {
			if (list.size() > 0) addDocs((ArrayList<WaterDoc>) list);
			l.onWorkDone();
		});
	}
}
