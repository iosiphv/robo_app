package com.vm.docs.income;

import android.arch.lifecycle.ViewModel;

import com.vm.adapter.AdapterDocsContent;

import dao.pojo.doc.IncomeDoc;

public class IncomeDocVM extends ViewModel {
	private static final String TAG = "IncomeDocVM";

	private final IncomeDoc doc;

	private AdapterDocsContent adapterContent;

	public IncomeDocVM(IncomeDoc doc) {
		this.doc = doc;
		adapterContent = new AdapterDocsContent(doc.getContentDocList());
	}

	public IncomeDoc getDoc() {
		return doc;
	}

	public AdapterDocsContent getAdapterContent() {
		return adapterContent;
	}

}
