package com.vm.docs.income;

import android.arch.lifecycle.ViewModel;
import android.util.ArrayMap;
import android.util.Log;
import android.widget.Filter;

import com.model.dao.rest.DAORESTIncomeDocsHelper;
import com.vm.adapter.AdapterDocsContent;
import com.vm.adapter.AdapterDocsHead;
import com.vm.adapter.filter.FilterIncomeDocs;
import com.vm.listeners.OnVMWorkDoneListener;

import java.util.ArrayList;

import dao.pojo.doc.IncomeDoc;

public class IncomeDocsVM extends ViewModel {
	private static final String DEBUG_TAG = "IncomeDocsVM";

	private ArrayList<IncomeDoc>    items;
	private DAORESTIncomeDocsHelper docsHelper;
	private AdapterDocsHead         adapter;
	private AdapterDocsHead         adapterFiltered;
	private Filter                  filter;

	private final ArrayMap<Integer, AdapterDocsContent> poolOfContentAdapters = new ArrayMap<>(10);

	public IncomeDocsVM() {
		items = new ArrayList<>(10);
		adapter = new AdapterDocsHead(this);
		filter = new FilterIncomeDocs(this);
		docsHelper = new DAORESTIncomeDocsHelper();

		Log.d(DEBUG_TAG, "IncomeDocsVM: Instancieted!!!");
	}

	public ArrayList<IncomeDoc> getItems() {
		return items;
	}

	public int getDocsCount() {
		return items.size();
	}

	public AdapterDocsHead getAdapter() {
		return adapter;
	}

	public void setAdapter(AdapterDocsHead adapter) {
		this.adapter = adapter;
	}

	public boolean addDocs(final ArrayList<IncomeDoc> docs) {
		Log.d(DEBUG_TAG, "add incomeDOc size:" + docs.size());
		int     i = this.items.size();
		boolean v = this.items.addAll(docs);

		for (IncomeDoc doc : items)
			poolOfContentAdapters.put(doc.getKoddoc(), new AdapterDocsContent(doc.getContentDocList()));
		adapter.notifyItemRangeInserted(i, docs.size());
		return v;
	}

	public boolean delDoc(int pos) {
		boolean val = items.remove(pos) != null;
		if (val) adapter.notifyItemRemoved(pos);
		return val;
	}

	public IncomeDoc getDocByPosition(int position) {
		return items.get(position);
	}

	public void fillDocsFromREST(final OnVMWorkDoneListener l) {
		docsHelper.getDocs(list -> {
			if (list.size() > 0) addDocs((ArrayList<IncomeDoc>) list);
			l.onWorkDone();
		});
	}

	public void clearData() {
		int i = items.size();
		items.clear();
		getAdapter().notifyItemRangeRemoved(0, i);
	}

	public void filter(String query) {
		if (filter == null) {
			filter = new FilterIncomeDocs(this);
			filter.filter(query);
		}
	}

	public AdapterDocsContent getContentAdapter(int pos) {
		return poolOfContentAdapters.get(items.get(pos).getKoddoc());
	}
}
