package com.vm.listeners.anim;

public interface OnRCItemBeahaviorByScrollDirection {

	void onScrolledToTop();
	void onScrolledToBottom();

	void onScrolledUp();
	void onScrolledDown();

}
