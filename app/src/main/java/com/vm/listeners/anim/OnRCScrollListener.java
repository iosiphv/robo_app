package com.vm.listeners.anim;

import android.support.v7.widget.RecyclerView;

public class OnRCScrollListener extends RecyclerView.OnScrollListener {
	private static final String TAG = "OnRCScrollListener";
	private OnRCItemBeahaviorByScrollDirection behavior;

	public OnRCScrollListener(OnRCItemBeahaviorByScrollDirection behavior) {
		this.behavior = behavior;
	}

	@Override
	public void onScrolled(RecyclerView rc, int dx, int dy) {
		if (!rc.canScrollVertically(-1)) behavior.onScrolledToTop();
		else if (!rc.canScrollVertically(1)) behavior.onScrolledToBottom();

		if (dy < 0) behavior.onScrolledUp();
		else if (dy > 0) behavior.onScrolledDown();
	}
}
