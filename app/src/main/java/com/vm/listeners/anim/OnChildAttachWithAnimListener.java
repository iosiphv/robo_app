package com.vm.listeners.anim;

import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.concurrent.atomic.AtomicLong;

public class OnChildAttachWithAnimListener
		implements RecyclerView.OnChildAttachStateChangeListener {
	private SpringAnimation animOnAttach;
	private AtomicLong startVal;
	private static final String TAG = "ANIM_ATTACH";

	public OnChildAttachWithAnimListener(AtomicLong startVal) {
		this.startVal = startVal;
	}

	@Override
	public void onChildViewAttachedToWindow(View view) {
		view.clearAnimation();

		animOnAttach = new SpringAnimation(view, DynamicAnimation.ALPHA, 1);
		animOnAttach.setStartValue(0);
		animOnAttach.getSpring().setDampingRatio(SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY);
		animOnAttach.getSpring().setStiffness(SpringForce.STIFFNESS_LOW);
		animOnAttach.start();

		Log.d(TAG, "startVal:" + startVal.floatValue());
	}

	@Override
	public void onChildViewDetachedFromWindow(View view) {
		view.clearAnimation();
	}
}
