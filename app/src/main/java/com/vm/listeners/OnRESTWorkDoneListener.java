package com.vm.listeners;

import java.util.ArrayList;

import dao.pojo.doc.Doc;

/**
 * Created by ovt10 on 24.02.2017.
 */
public interface OnRESTWorkDoneListener {
	void onRestWorkDone(ArrayList<? extends Doc> list);
}
