package com.vm.listeners;

public interface OnVMWorkDoneListener {
	void onWorkDone();
}
