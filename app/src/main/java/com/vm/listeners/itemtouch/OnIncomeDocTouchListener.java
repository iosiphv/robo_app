package com.vm.listeners.itemtouch;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

public class OnIncomeDocTouchListener implements RecyclerView.OnItemTouchListener {
	private final GestureDetectorCompat gestureDetector;
	private OnRcItemClickListener listener;

	public OnIncomeDocTouchListener(Context context,
	                                OnRcItemClickListener listener) {
		this.gestureDetector =
				new GestureDetectorCompat(context,
				                          new SimpleOnGestureListener() {
					                          @Override
					                          public boolean onSingleTapUp(MotionEvent e) {
						                          return true;
					                          }
				                          });
		this.listener = listener;
	}

	@Override
	public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
		View v = rv.findChildViewUnder(e.getX(), e.getY());
		if (v != null && gestureDetector.onTouchEvent(e) && (v instanceof CardView))
			listener.onClick(v, rv.getChildAdapterPosition(v));
		return false;
	}

	@Override
	public void onTouchEvent(RecyclerView rv, MotionEvent e) {

	}

	@Override
	public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

	}
}
