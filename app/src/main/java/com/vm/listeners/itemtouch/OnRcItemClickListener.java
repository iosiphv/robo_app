package com.vm.listeners.itemtouch;

import android.view.View;

public interface OnRcItemClickListener {

	void onClick(View view, int pos);

}
