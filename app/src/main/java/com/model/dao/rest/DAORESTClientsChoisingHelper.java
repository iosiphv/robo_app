package com.model.dao.rest;

import com.rest.clients.ClientsService;
import com.vm.listeners.OnRESTWorkDoneListener;

import java.util.ArrayList;

import dao.pojo.Client;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 09.10.2017.
 */

public class DAORESTClientsChoisingHelper {
	private final ClientsService clService;
	private final String TAG = DAORESTClientsChoisingHelper.class.getSimpleName();

	public DAORESTClientsChoisingHelper(ClientsService clService) {
		this.clService = clService;
	}

	public ArrayList<Client> getDocs(final OnRESTWorkDoneListener listener) {
		final ArrayList<Client> clients = new ArrayList<>(50);
//		clService
//				.getClients()
//				.enqueue(new GetClientsCallback(clients, listener));
/*DUMMY DATA*/
		for (int i = 0; i < 50; i++) {
			Client cl = new Client(i*11);
			cl.setGroupName("Group"+i);
			cl.setName("Name"+i);
			clients.add(cl);
		}

		return clients;
	}

	private class GetClientsCallback implements Callback<ArrayList<Client>> {

		private ArrayList<Client> clients;
		private OnRESTWorkDoneListener listener;

		private GetClientsCallback(final ArrayList<Client> clients,
		                           final OnRESTWorkDoneListener listener) {
			this.clients = clients;
			this.listener = listener;
		}

		@Override
		public void onResponse(Call<ArrayList<Client>> call, Response<ArrayList<Client>> response) {
			if (response.isSuccessful()) {
				clients.addAll(response.body());
//				listener.onRestWorkDone((ArrayList<? extends Doc>) clients);
			}
		}

		@Override
		public void onFailure(Call<ArrayList<Client>> call, Throwable t) {
		}
	}

}
