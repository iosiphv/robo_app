package com.model.dao.rest;

import com.vm.listeners.OnRESTWorkDoneListener;

public interface DAODocs<T> {
	boolean insert(T doc);

	boolean update(T doc);

	boolean remove(T doc);

	boolean getDocs(OnRESTWorkDoneListener listener);
}

