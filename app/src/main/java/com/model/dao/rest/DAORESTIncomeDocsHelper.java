package com.model.dao.rest;

import android.icu.util.GregorianCalendar;
import android.icu.util.LocaleData;
import android.util.Log;

import com.rest.RetrofitHelper;
import com.rest.income.IncomeDocsService;
import com.vm.listeners.OnRESTWorkDoneListener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import dao.pojo.Client;
import dao.pojo.KodM;
import dao.pojo.doc.ContentDoc;
import dao.pojo.doc.Doc;
import dao.pojo.doc.IncomeDoc;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DAORESTIncomeDocsHelper implements DAODocs<IncomeDoc> {
	private static final String DEBUG_TAG = "DAORESTIncomeDocsHelper";

	private final IncomeDocsService docsService = RetrofitHelper.getRetrofit()
	                                                            .create(IncomeDocsService.class);

	@Override
	public boolean insert(IncomeDoc doc) {
		return false;
	}

	@Override
	public boolean update(IncomeDoc doc) {
		return false;
	}

	@Override
	public boolean remove(IncomeDoc doc) {
		return false;
	}

	@Override
	public boolean getDocs(final OnRESTWorkDoneListener listener) {
		/*DUMMY DATA*/
		ArrayList<IncomeDoc> body = new ArrayList<>(10);
		for (int i = 0; i < 10; i++) {
			IncomeDoc iDoc = new IncomeDoc();
			iDoc.setDate(new Date(Calendar.getInstance().getTimeInMillis()));
			Client income = new Client(i * 11);
			income.setName("INC" + i);
			income.setGroupName("GROUP_INC_" + i);
			iDoc.setIncome(income);

			iDoc.setKoddoc(i);
			iDoc.setNum(i * 7);

			Client outgone = new Client(i * 5);
			income.setName("gone" + i);
			income.setGroupName("GROUP_gone_" + i);
			iDoc.setOutgone(outgone);

			iDoc.setTypeOfTransfer(Doc.TYPE_TRANSFER.NEW);
			ArrayList<ContentDoc> contentDocList = new ArrayList<>(1);
			for (int j = 0; j < 10; j++) {
				ContentDoc cd = new ContentDoc();

				Client cl = new Client(j * 11);
				cl.setName("item_INC" + i + " " + j);
				cl.setGroupName("item_GROUP_INC_" + +i + " " + j);

				KodM kodM = new KodM(cl);
				kodM.setInner(String.valueOf(j));
				kodM.setInv(String.valueOf(j));
				kodM.setSerial(String.valueOf(j));

				cd.setKodM(kodM);
				cd.setQuant(i);
				cd.setSum(j);
				cd.setComment("comment " + i + " " + j);

				contentDocList.add(cd);
			}

			iDoc.setContentDocList(contentDocList);
			body.add(iDoc);
		}
		listener.onRestWorkDone(body);

//		Call<ArrayList<IncomeDoc>> byCurrMonth = docsService.getDocsByCurrMonth();
//
//		byCurrMonth.enqueue(new Callback<ArrayList<IncomeDoc>>() {
//			@Override
//			public void onResponse(Call<ArrayList<IncomeDoc>> call,
//			                       Response<ArrayList<IncomeDoc>> response
//			) {
//
//				if (response.isSuccessful()) {
//					ArrayList<IncomeDoc> body = response.body();
//					Log.d(DEBUG_TAG, "getDocs: in callback" + body.size());
//					listener.onRestWorkDone(body);
//				} else Log.e(DEBUG_TAG, String.valueOf(response.code()));
//			}
//
//			@Override
//			public void onFailure(Call<ArrayList<IncomeDoc>> call, Throwable t) {
//				listener.onRestWorkDone(new ArrayList<IncomeDoc>(1));
//			}
//		});
		return true;
	}
}
