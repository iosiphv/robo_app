package com.model.dao.rest;

import android.util.Log;

import com.rest.RetrofitHelper;
import com.rest.water.WaterDocsService;
import com.vm.listeners.OnRESTWorkDoneListener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;

import dao.pojo.Client;
import dao.pojo.KodM;
import dao.pojo.doc.ContentDoc;
import dao.pojo.doc.ContentWaterDoc;
import dao.pojo.doc.Doc;
import dao.pojo.doc.IncomeDoc;
import dao.pojo.doc.WaterDoc;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DAORESTWaterDocsHelper implements DAODocs<WaterDoc> {
	private static final String TAG = "DAORESTWaterDocsHelper";

	private final WaterDocsService docsService = RetrofitHelper.getRetrofit()
	                                                           .create(WaterDocsService.class);

	@Override
	public boolean insert(WaterDoc doc) {
		return false;
	}

	@Override
	public boolean update(WaterDoc doc) {
		return false;
	}

	@Override
	public boolean remove(WaterDoc doc) {
		return false;
	}

	public void clientMakePayment(int koddoc, int kodM, double sumPay,
	                              final OnRESTWorkDoneListener listener
	) {
		Log.i(TAG, "try make PAY!!!");
		listener.onRestWorkDone(new ArrayList<>(1));

//		docsService.performClientPay(koddoc, kodM, sumPay)
//		           .enqueue(
//				           new Callback<Void>() {
//					           @Override
//					           public void onResponse(Call<Void> call, Response<Void> response) {
//						           Log.i(TAG, "try make PAY!!!");
//					           }
//
//					           @Override
//					           public void onFailure(Call<Void> call, Throwable t) {
//						           Log.e(TAG, t.getLocalizedMessage());
//					           }
//				           }
//		           );
	}

	public WaterDoc createNewDoc(final OnRESTWorkDoneListener listener) {
		final WaterDoc retDoc = new WaterDoc();
		ArrayList<Doc> list   = new ArrayList<>();
		list.add(retDoc);
		listener.onRestWorkDone(list);
//		docsService.createNewDoc()
//		           .enqueue(
//				           new Callback<WaterDoc>() {
//					           @Override
//					           public void onResponse(Call<WaterDoc> call, Response<WaterDoc> response) {
//						           if (response.isSuccessful()) {
//							           Log.d(TAG, response.toString());
////							           listener.onRestWorkDone(Collections.list(response.body()));
//						           }
//					           }
//
//					           @Override
//					           public void onFailure(Call<WaterDoc> call, Throwable t) {
//						           Log.e(TAG, t.getLocalizedMessage());
//					           }
//				           }
//		           );
		return retDoc;
	}

	@Override
	public boolean getDocs(final OnRESTWorkDoneListener listener) {
		/*DUMMY DATA*/
		ArrayList<WaterDoc> body = new ArrayList<>(10);
		for (int i = 0; i < 10; i++) {
			WaterDoc iDoc   = new WaterDoc();
			Client   income = new Client(i * 11);
			income.setName("INC" + i);
			income.setGroupName("GROUP_INC_" + i);
			iDoc.setData(new Date(Calendar.getInstance().getTimeInMillis()));
			iDoc.setKoddoc(i);
			iDoc.setSumma((long) (i * 3));
			ArrayList<ContentWaterDoc> contentDocList = new ArrayList<>(1);
			for (int j = 0; j < 10; j++) {
				Client client = new Client(j);
				client.setName("water cli " + j);
				client.setGroupName("group wat cli " + j);

				ContentWaterDoc cd = new ContentWaterDoc(client, (double) (j * i));
				contentDocList.add(cd);
			}
			iDoc.setContentDocList(contentDocList);
			body.add(iDoc);
		}
		listener.onRestWorkDone(body);

//		docsService
//				.getAllDocs()
//				.enqueue(
//						new Callback<ArrayList<WaterDoc>>() {
//							@Override
//							public void onResponse(Call<ArrayList<WaterDoc>> call,
//							                       Response<ArrayList<WaterDoc>> response) {
//								if (response.isSuccessful()) {
//									final ArrayList<WaterDoc> body = response.body();
//									Log.d(TAG, "onResponse: " + body.size());
//									listener.onRestWorkDone(body);
//								} else Log.e(TAG, "code error:" + response.code());
//							}
//
//							@Override
//							public void onFailure(Call<ArrayList<WaterDoc>> call, Throwable t) {
//								Log.e(TAG, "onFailure:", t);
//								listener.onRestWorkDone(new ArrayList<WaterDoc>(0));
//							}
//						}
//				);
		return true;
	}
}
